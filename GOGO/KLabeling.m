function GG = KLabeling(XC,YC,XF,YF,tempoint)
% XC：X軸位置命令、YC：Y軸位置命令、XF：X軸位置迴授、YF：Y軸位置迴授
% % BadPathExam=corrcoef(sqrt(XC.^2+YC.^2),sqrt(XF.^2+YF.^2)); % abs(sqrt(XC.^2+YC.^2)-sqrt(XF.^2+YF.^2))<2
% % if BadPathExam(1,2)<0.8
% %     GG=[0 0 0 0 0 0 0 0 0 0 0 0];
% % else
%% 表面粗糙度輸入
sampling_range=6;  % 設計取樣長度% 設計取樣長度% 設計取樣長度% 設計取樣長度% 設計取樣長度
Porder=0;           % 多項式擬合趨勢項階數
average_points=1;   % Ry前幾大和前幾小值取平均
vthita=0;           % 位置命令與迴授向量角度大於此值才納入計算(大多小於5度，幾乎大於2度)
Bpoints=20;
CornerX=[1 1 -1];% 設定x理想點
CornerY=[-1 2 0];% 設定y理想點

%% 轉角誤差(論文)

% CornerX=[1 1 -1];% 設定x理想點
% CornerY=[-1 2 0];% 設定y理想點
% % CornerX=[2 2 0];% 設定x理想點 FOR 610nr1_0-10-1000.csv
% % CornerY=[0 3 1];% 設定y理想點
% 
% for j=1:3   % 加工指標計算點
%     [Corner_ERR_temp(j) Corner_point_temp(j)]=min(sqrt((XF-CornerX(j)).^2+(YF-CornerY(j)).^2));
% end
% 
% [p_corner,s_corner,mu_corner] = polyfit(XF(Corner_point_temp(2):Corner_point_temp(3)),YF(Corner_point_temp(2):Corner_point_temp(3)),1);
% 
% for j=1:3   % 加工指標計算點
%     if j==1
%         [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((XF-max(XF(Corner_point_temp(j)-sampling_range:Corner_point_temp(j)+sampling_range))).^2+...
%                                                  (YF-min(YF(Corner_point_temp(j)-sampling_range:Corner_point_temp(j)+sampling_range))).^2));
%     elseif j==2
%         yy4 = polyval(p_corner,max(XF(Corner_point_temp(j)-sampling_range:Corner_point_temp(j)+sampling_range)),[],mu_corner); % 擬合初步1維的表粗剖面，Porder取2階(因為轉角大致為2階曲線)
%         [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((XF-max(XF(Corner_point_temp(j)-sampling_range:Corner_point_temp(j)+sampling_range))).^2+(YF-yy4).^2));
%     elseif j==3
%         yy5 = polyval(p_corner,min(XF(Corner_point_temp(j)-sampling_range:Corner_point_temp(j)+sampling_range)),[],mu_corner); % 擬合初步1維的表粗剖面，Porder取2階(因為轉角大致為2階曲線)
%         [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((XF-min(XF(Corner_point_temp(j)-sampling_range:Corner_point_temp(j)+sampling_range))).^2+(YF-yy5).^2));
%     else
%         [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((XF-CornerX(j)).^2+(YF-CornerY(j)).^2));
%     end
% end
% for j=1:3
%     Corner_ERR(j)=norm([XF(Corner_point_temp2(j))-CornerX(j) YF(Corner_point_temp2(j))-CornerY(j)]);
% end

%% 轉角誤差(方案B)

for j=1:3   % 加工指標計算點
    [Corner_ERR_temp(j) Corner_point_temp(j)]=min(sqrt((XF-CornerX(j)).^2+(YF-CornerY(j)).^2));
    
    if Corner_point_temp(j)<4
        Corner_point_temp(j)=4;
    elseif Corner_point_temp(j)+3>length(XC)
        Corner_point_temp(j)=length(XC)-3;
    end
    
    QFxy(1:Bpoints+1,2*j-1:2*j)=Funct_Bezier(XF(Corner_point_temp(j)-3:Corner_point_temp(j)),YF(Corner_point_temp(j)-3:Corner_point_temp(j)),Bpoints,tempoint);
    QFxy(Bpoints+2:2*(Bpoints+1),2*j-1:2*j)=Funct_Bezier(XF(Corner_point_temp(j):Corner_point_temp(j)+3),YF(Corner_point_temp(j):Corner_point_temp(j)+3),Bpoints,tempoint);
    QCxy(1:Bpoints+1,2*j-1:2*j)=Funct_Bezier(XC(Corner_point_temp(j)-3:Corner_point_temp(j)),YC(Corner_point_temp(j)-3:Corner_point_temp(j)),Bpoints,tempoint);
    QCxy(Bpoints+2:2*(Bpoints+1),2*j-1:2*j)=Funct_Bezier(XC(Corner_point_temp(j):Corner_point_temp(j)+3),YC(Corner_point_temp(j):Corner_point_temp(j)+3),Bpoints,tempoint);
end

[p_corner,s_corner,mu_corner] = polyfit(XF(Corner_point_temp(2):Corner_point_temp(3)),YF(Corner_point_temp(2):Corner_point_temp(3)),1);

for j=1:3   % 加工指標計算點
    if j==1
        [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((QFxy(:,2*j-1)-max(QFxy(:,2*j-1))).^2+(QFxy(:,2*j)-min(QFxy(:,2*j))).^2));
    elseif j==2
        yy4 = polyval(p_corner,max(QFxy(:,2*j-1)),[],mu_corner); % 擬合初步1維的表粗剖面，Porder取2階(因為轉角大致為2階曲線)
        [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((QFxy(:,2*j-1)-max(QFxy(:,2*j-1))).^2+(QFxy(:,2*j)-yy4).^2));
    elseif j==3
        yy5 = polyval(p_corner,min(QFxy(:,2*j-1)),[],mu_corner); % 擬合初步1維的表粗剖面，Porder取2階(因為轉角大致為2階曲線)
        [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((QFxy(:,2*j-1)-min(QFxy(:,2*j-1))).^2+(QFxy(:,2*j)-yy5).^2));
    else
        [Corner_ERR_temp2(j) Corner_point_temp2(j)]=min(sqrt((QFxy(:,2*j-1)-CornerX(j)).^2+(QFxy(:,2*j)-CornerY(j)).^2));
    end
end
for j=1:3
    Corner_ERR(j)=norm([QFxy(Corner_point_temp2(j),2*j-1)-CornerX(j) QFxy(Corner_point_temp2(j),2*j)-CornerY(j)]);
end

%% 表面粗糙度
counting(:,:)=zeros(2,2);
% XXC(1:3,:)=zeros(length(XC(Corner_point_temp(1)-sampling_range:Corner_point_temp(1)+sampling_range)));
% YYC(1:3,:)=zeros(length(YC(Corner_point_temp(1)-sampling_range:Corner_point_temp(1)+sampling_range)));
% XXF(1:3,:)=zeros(length(XF(Corner_point_temp(1)-sampling_range:Corner_point_temp(1)+sampling_range)));
% YYF(1:3,:)=zeros(length(YF(Corner_point_temp(1)-sampling_range:Corner_point_temp(1)+sampling_range)));
for k=1:3   % 加工指標計算點
%%%論文
range1=Corner_point_temp(k)-sampling_range;range2=Corner_point_temp(k)+sampling_range;
if range2>length(XC)
    XXC(k,1:length(XC)-range1+1)=XC(range1:length(XC));YYC(k,1:length(XC)-range1+1)=YC(range1:length(XC));
    XXF(k,1:length(XC)-range1+1)=XF(range1:length(XC));YYF(k,1:length(XC)-range1+1)=YF(range1:length(XC));
    XXC(k,length(XC)-range1+2:range2-range1+1)=XC(length(XC));YYC(k,length(XC)-range1+2:range2-range1+1)=YC(length(XC));
    XXF(k,length(XC)-range1+2:range2-range1+1)=XF(length(XC));YYF(k,length(XC)-range1+2:range2-range1+1)=YF(length(XC));
elseif range1<1
    range2=range2+(1-range1);
    range1=1;
    XXC(k,:)=XC(range1:range2);YYC(k,:)=YC(range1:range2);XXF(k,:)=XF(range1:range2);YYF(k,:)=YF(range1:range2);
else
    XXC(k,:)=XC(range1:range2);YYC(k,:)=YC(range1:range2);XXF(k,:)=XF(range1:range2);YYF(k,:)=YF(range1:range2);
end

%%%方案B
% XXC(k,:)=QCxy(:,2*k-1)';
% YYC(k,:)=QCxy(:,2*k)';
% XXF(k,:)=QFxy(:,2*k-1)';
% YYF(k,:)=QFxy(:,2*k)';

% vlf(k,:)=sqrt((XXF(k,:)).^2+(YYF(k,:)).^2);
% % vlc(k,:)=sqrt((XXC(k,:)-CornerX(k)).^2+(YYC(k,:)-CornerY(k)).^2);
% if k==2 || k==3
%     xlc(k,:)=[0:length(vlf(k,:))-1];[plc,slc,mulc] = polyfit(xlc(k,:),vlf(k,:),9);
% else
%     xlc(k,:)=[0:length(vlf(k,:))-1];[plc,slc,mulc] = polyfit(xlc(k,:),vlf(k,:),10);
% end
% cly(k,:) = polyval(plc,xlc(k,:),[],mulc); % 擬合初步1維的表粗剖面，Porder取2階(因為轉角大致為2階曲線)
% fuck(k,:)=abs(vlf(k,:)-cly(k,:));
% % figure(87+k);hold on;plot(cly(k,:),'-o');%plot(vlf(k,:),'-x');
% % figure(38+k);hold on;plot(fuck(k,:),'-*');
% % gg(:,k)=sort(fuck(k,:),'descend');
% % Corner_Ra(k)=sum(gg(1:3,k)-gg(length(gg)-2:length(gg),k))/3*1000;
% Corner_Ra(k)=sum(fuck(k,:));



% 生成表面粗糙度量測曲線
ADD(1)=0;
for n=1:length(XXC(k,:))-1
    VC(n,:)=[XXC(k,n+1)-XXC(k,n) YYC(k,n+1)-YYC(k,n) 0]; % 位置命令向量(第3維因為XY平面故為0)
    VF(n,:)=[XXF(k,n+1)-XXF(k,n) YYF(k,n+1)-YYF(k,n) 0]; % 回授位置向量(第3維因為XY平面故為0)
    direction(n,:)=sign(cross(VC(n,:),VF(n,:))); % 位置命令向量與回授位置向量取外積，取其第3維之正負號。
%     VD(n)=direction(n,3)*norm(sqrt(norm(VC(n,:))^2+norm(VF(n,:))^2-... % 餘弦定理算出位置命令向量和回授位置向量之向量差長度
%         2*norm(VC(n,:))*norm(VF(n,:))*dot(VC(n,:),VF(n,:))/norm(VC(n,:))/norm(VF(n,:))));  

    VD(n,k)=direction(n,3)*norm(VC(n,:)-VF(n,:));
    
%     if acos(dot(VC(n,:),VF(n,:))/norm(VC(n,:))/norm(VF(n,:)))*180/pi>=vthita
%          ADD(n)=VD(n); % 將每個正負方向的向量差長度累積起來形成初步1維的表粗剖面，"ADD(n+1)=ADD(n)+VD(n);"換掉了！
        counting(n,k)=1;
%     else
%         ADD(n+1)=ADD(n);
%         counting(n,k)=0;
%     end

%     df(n)=abs(norm(VC(n,:))-norm(VF(n,:)));

%     VC(n,:)=[XXC(k,n+1)-XXC(k,n) YYC(k,n+1)-YYC(k,n)];
%     VF(n,:)=[XXF(k,n+1)-XXF(k,n) YYF(k,n+1)-YYF(k,n)];
% 
%     if XXC(k,n+1)-XXC(k,n)==0 && YYC(k,n+1)-YYC(k,n)==0 % 真的遇過，Ry20,20%取平均，VD，KAKINO銳角。
%        VC(n,:)=[eps eps];
%     end
%     if XXF(k,n+1)-XXF(k,n)==0 && YYF(k,n+1)-YYF(k,n)==0 % 還沒遇過，為了保險，Ry20,20%取平均，VD，KAKINO銳角。
%        VF(n,:)=[eps eps];
%     end
% 
%     VD(n,k)=(VC(n,1)*VF(n,2)-VC(n,2)*VF(n,1))/norm(VC(n,:));
%     % direction(n)=sign(VD(n));
%     counting(n,k)=1;
%     % vv(n)=direction(n)*dot(VC(n,:),VF(n,:))/norm(VC(n,:));
%     % VV(n)=direction(n)*VD(n);
%     % ADD(n+1)=ADD(n)+VD(n);
%     ADD(n)=VD(n);

end
% 生成表面粗糙度剖面圖
% x_Ra=[0:length(ADD)-1];ty=ADD;
% [p_Ra,s_Ra,mu_Ra] = polyfit(x_Ra,ty,Porder);f_y = polyval(p_Ra,x_Ra,[],mu_Ra); % 擬合初步1維的表粗剖面，Porder取2階(因為轉角大致為2階曲線)
% y_Ra = ty-f_y; % 扣除轉角趨勢形成較完整之表粗剖面

% % % 看完整之表粗剖面
% if k==1 % 選擇第幾個量測點
%     figure(87);hold on;grid on;set(gca,'fontsize',16);title(['Ra20第' int2str(k) '量測點']);xlabel('圈數');ylabel('計算點');plot(VD);%plot(x_Ra,y_Ra);
%     hold off;
% end

% Ry值
% y_sorted=sort(y_Ra);
% % Corner_Ry(k)=abs((sum(y_sorted(length(y_Ra)-(average_points-1):length(y_Ra)))/average_points)-(sum(y_sorted(1:average_points))/average_points))*1000;
% Corner_Ry(k)=abs(max(y_Ra)-min(y_Ra))*1000;


% Ra值積分除於長度
% fit=csapi(x_Ra,y_Ra);
% syms xx;X=[xx^3;xx^2;xx;1];
% for n=1:length(fit.coefs(:,1))
%     Intofints(n)=abs(int(fit.coefs(n,:)*X,0,fit.breaks(n+1)-fit.breaks(n)));    
% end
% Corner_Ra(k)=double(sum(Intofints)/abs(max(x_Ra)-min(x_Ra)))*1000;

VDde(:,k)=sort(VD(:,k),'descend');
% Corner_Ra(k)=sum(VDde(1:3,k))/3;
% Corner_Ra(k)=sum(abs(VD(:,k)));
Corner_Ra(k)=sum(VDde(1:3,k)-VDde(length(VDde)-2:length(VDde),k))/3*1000;

% Corner_Ra(k)=max(sqrt(TCMDX.^2+TCMDY.^2));
end
%%
[corner spot]=max(Corner_ERR);
% 
% GG=[Corner_ERR(1) Corner_Ra(1) Corner_Ry(1) Corner_ERR(2) Corner_Ra(2) Corner_Ry(2)...
%     Corner_ERR(3) Corner_Ra(3) Corner_Ry(3) Corner_ERR(4) Corner_Ra(4) Corner_Ry(4)...
%     Corner_ERR(5) Corner_Ra(5) Corner_Ry(5) TIME spot area place];
%%
[Ra place]=max(Corner_Ra);
%% 加工時間
TIME=length(XF)*3;
%%
GG=[Corner_ERR(1) Corner_Ra(1) Corner_ERR(2) Corner_Ra(2) Corner_ERR(3) Corner_Ra(3)  ...
    TIME spot place sum(counting(:,1)) sum(counting(:,2)) sum(counting(:,3))];
% end
% [Ry place]=max(Corner_Ry);
% GG=[Corner_ERR(1) Corner_Ry(1) Corner_ERR(2) Corner_Ry(2) Corner_ERR(3) Corner_Ry(3)...
%     Corner_ERR(4) Corner_Ry(4) Corner_ERR(5) Corner_Ry(5) TIME spot place sum(counting(:,1)) sum(counting(:,2)) sum(counting(:,3)) sum(counting(:,4)) sum(counting(:,5))];

% GG=[max(Corner_ERR) max(Corner_Ra) TIME spot place sum(counting(:,1)) sum(counting(:,2)) sum(counting(:,3)) sum(counting(:,4)) sum(counting(:,5))];