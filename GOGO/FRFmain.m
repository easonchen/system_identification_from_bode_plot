clear;clc;close all;
dbstop if error ;
nb_times=1;
cc=1;
cc2=0;
cc3=0;
%%
% A=importdata('ID610NR1=0-100-2000_ID610NR2=2000_RE.csv', ';');
A=importdata('id610nr1_0-2000_RE_feedforwardOFF2.csv', ';');  % ID610NR1=0-100-2000_ID610NR2=2000_RE.csv
% B=importdata('x position loop frf_bw8.4Hz_am20db104.9hz_pm78.5d6.4hz_raw3.csv', ';');
% C=importdata('y position loop frf_bw6.9Hz_am17.9db49.8hz_pm72.2d4.3hz_raw3.csv', ';');
POSCX=A.data(:,2);POSCY=A.data(:,3);POS3DX=A.data(:,4);POS3DY=A.data(:,5);
%%
DataPerCycle=PathSeparator2(POSCX,POSCY,-1,-1,100);
['共判斷出： ' int2str(length(DataPerCycle)-3) '圈，計時開始']
%%
tempoint=1;
    for n=1:length(DataPerCycle)-4 % Number of Cycles，計算需要:-1；第一圈不要:-2；最後一圈不要：-3
        % 原始資料第一圈和最後一圈有刻意多跑一圈以確保資料分割沒有包含道結束時的暫態:-2。
        % 分離出來的每圈資料
        PerformanceReal(n,:)=KLabeling(POSCX(DataPerCycle(n+2):DataPerCycle(n+3)),... % 從"n+1"開始是因為第一圈是原點到起點;
                                   POSCY(DataPerCycle(n+2):DataPerCycle(n+3)),... % "n+2"則是第一圈是原點到起點和第一圈再多跑一次
                                   POS3DX(DataPerCycle(n+2):DataPerCycle(n+3)),...% "n+3"則是第一圈再多跑一次
                                   POS3DY(DataPerCycle(n+2):DataPerCycle(n+3)),tempoint);... % "n+2"則是第一圈是原點到起點和第一圈再多跑一次
        tempoint=tempoint+1;
    end
%%
R0(1)=0; % 相關係數初始值
maginaleffect(1,:)=[0 0 0 0];
BadPathExam(1)=0;
hwait=waitbar(cc3/18226,'程式執行中請勿關機，否則將對電腦造成嚴重毀損！');
for denX=5
    for numX=1:denX
        for denY=5
            for numY=1:denY

[tfXnum tfXden]=idenmodel('x position loop frf_bw8.8Hz_am20.4db106.5hz_pm77.3d7.0hz_amp_feedforwardoff_dcmoff2.csv',...
    'x position loop frf_bw8.8Hz_am20.4db106.5hz_pm77.3d7.0hz_pha_feedforwardoff_dcmoff2.csv',numX,denX);
[tfYnum tfYden]=idenmodel('y position loop frf_bw7.8Hz_am20.5db39.2hz_pm67.7d4.5hz_amp_feedforwardoff_dcmoff.csv',...
    'y position loop frf_bw7.8Hz_am20.5db39.2hz_pm67.7d4.5hz_pha_feedforwardoff_dcmoff.csv',numY,denY);

SimOutputX=lsim(tfXnum,tfXden,POSCX,A.data(:,1)./1e6);
SimOutputY=lsim(tfYnum,tfYden,POSCY,A.data(:,1)./1e6);
% SimOutputX=SimOutputX*max(abs(POS3DX))/max(abs(SimOutputX));
% SimOutputY=SimOutputY*max(abs(POS3DY))/max(abs(SimOutputY));
hwait=waitbar(cc3/18226);
cc3=cc3+1;
% BadPathExam(cc3)=corr2(sqrt(POS3DX(DataPerCycle(end-3):DataPerCycle(end-2)).^2+POS3DY(DataPerCycle(end-3):DataPerCycle(end-2)).^2),...
%     sqrt(SimOutputX(DataPerCycle(end-3):DataPerCycle(end-2)).^2+SimOutputY(DataPerCycle(end-3):DataPerCycle(end-2)).^2));
% BadPathExamX(cc3)=corr2(POS3DX(DataPerCycle(end-3):DataPerCycle(end-2)),SimOutputX(DataPerCycle(end-3):DataPerCycle(end-2)));
% BadPathExamY(cc3)=corr2(POS3DY(DataPerCycle(end-3):DataPerCycle(end-2)),SimOutputY(DataPerCycle(end-3):DataPerCycle(end-2)));
%abs(sqrt(POS3DX.^2+POS3DY.^2)-sqrt(SimOutputX.^2+SimOutputY.^2))<2
BadPathExamX(cc3)=max(abs(POS3DX-SimOutputX));BadPathExamY(cc3)=max(abs(POS3DY-SimOutputY));

                if BadPathExamX(cc3)<1 && BadPathExamY(cc3)<1 % 0.92以下就會GG
                    % SimOutputX=SimOutputX*-1*max(abs(POS3DX))/max(abs(SimOutputX));
                    % SimOutputY=SimOutputY*-1*max(abs(POS3DY))/max(abs(SimOutputY));

                    tempoint=1;
                    for n=1:length(DataPerCycle)-4 % Number of Cycles，計算需要:-1；第一圈不要:-2；最後一圈不要：-3
                        % 原始資料第一圈和最後一圈有刻意多跑一圈以確保資料分割沒有包含道結束時的暫態:-2。
                        % 分離出來的每圈資料
                        PerformanceSimu(n,:)=KLabeling(POSCX(DataPerCycle(n+2):DataPerCycle(n+3)),... % 從"n+1"開始是因為第一圈是原點到起點;
                                                POSCY(DataPerCycle(n+2):DataPerCycle(n+3)),... % "n+2"則是第一圈是原點到起點和第一圈再多跑一次
                                                SimOutputX(DataPerCycle(n+2):DataPerCycle(n+3)),...% "n+3"則是第一圈再多跑一次
                                                SimOutputY(DataPerCycle(n+2):DataPerCycle(n+3)),tempoint);... % "n+2"則是第一圈是原點到起點和第一圈再多跑一次
                        tempoint=tempoint+1;
                    end
                    cc2=cc2+1;
                    RR(cc2)=corr2(PerformanceReal(:,1:6),PerformanceSimu(:,1:6)); % 以加工指標相關係數作為階數決定的依據
                    
                else
                    cc2=cc2+1;
                    RR(cc2)=-1;
                end
% RR=corr2(sqrt(POS3DX.^2+POS3DY.^2),sqrt(SimOutputX.^2+SimOutputY.^2)); % 以時域響應相關係數作為階數決定的依據

% RRR(denX,numX+1,denY,numY+1)=RR;

                if RR(cc2)>R0(cc)
                    R0(cc+1)=RR(cc2);
                    maginaleffect(cc,:)=[numX denX numY denY];
                    best_numX=tfXnum;best_denX=tfXden;
                    best_numY=tfYnum;best_denY=tfYden;
                    PerformanceSimu_best=PerformanceSimu;
                    cc=cc+1;
                end

            end
        end
    end
end
close(hwait); % 註意必須添加close函數
if R0==0
    ['找不到R2>0的模型']
else
% ind = find(RRR==max(RRR(:)));
% [i,j,k,l] = ind2sub([denX numX denY numY],ind);
%%
SimOutputX_best=lsim(best_numX,best_denX,POSCX,A.data(:,1)./1e6);
SimOutputY_best=lsim(best_numY,best_denY,POSCY,A.data(:,1)./1e6);
best_GainX=max(abs(POS3DX))/max(abs(SimOutputX_best));
best_GainY=max(abs(POS3DY))/max(abs(SimOutputY_best));
SimOutputX_best2=SimOutputX_best*best_GainX;
SimOutputY_best2=SimOutputY_best*best_GainY;
Rx=corr2(POS3DX,SimOutputX_best);Ry=corr2(POS3DY,SimOutputY_best);
                    tempoint=1;
                    for n=1:length(DataPerCycle)-4 % Number of Cycles，計算需要:-1；第一圈不要:-2；最後一圈不要：-3
                        % 原始資料第一圈和最後一圈有刻意多跑一圈以確保資料分割沒有包含道結束時的暫態:-2。
                        % 分離出來的每圈資料
                        PerformanceSimu_best2(n,:)=KLabeling(POSCX(DataPerCycle(n+2):DataPerCycle(n+3)),... % 從"n+1"開始是因為第一圈是原點到起點;
                                                POSCY(DataPerCycle(n+2):DataPerCycle(n+3)),... % "n+2"則是第一圈是原點到起點和第一圈再多跑一次
                                                SimOutputX_best2(DataPerCycle(n+2):DataPerCycle(n+3)),...% "n+3"則是第一圈再多跑一次
                                                SimOutputY_best2(DataPerCycle(n+2):DataPerCycle(n+3)),tempoint);... % "n+2"則是第一圈是原點到起點和第一圈再多跑一次
                        tempoint=tempoint+1;
                    end
%%
    figure(21);set(gca,'fontsize',16);grid on;hold on;axis equal;
    plot(POSCX,POSCY,'o-','LineWidth',1);
    plot(POS3DX,POS3DY,'o-','LineWidth',1);
    plot(SimOutputX_best,SimOutputY_best,'x-','LineWidth',1);
    plot(SimOutputX_best2,SimOutputY_best2,'x-','LineWidth',1);
    title(["System Identification's R^2= ",num2str(R0(end))],'Color', 'm');xlabel('X axis','Color', 'k');ylabel('Y axis','Color', 'k');
    simulated_scale=['Simulated Path(' 'Xgain=' num2str(best_GainX) '; Ygain=' num2str(best_GainY) ')'];
    legend({'Command Path','Real Path','Simulated Path',simulated_scale},'TextColor', 'k','location','best');hold off;

    figure(22);subplot(2,1,1);set(gca,'fontsize',16);grid on;hold on;
    plot(A.data(:,1),POS3DX,'o-','LineWidth',1);plot(A.data(:,1),SimOutputX_best,'x-','LineWidth',1);plot(A.data(:,1),SimOutputX_best2,'x-','LineWidth',1);
    title(['X axis',num2str(Rx)],'Color', 'm');xlabel('Time(msec)','Color', 'k');ylabel('Length(mm)','Color', 'k');
    simulated_scaleX=['Simulated Path(Xgain=' num2str(best_GainX) ')'];
    legend({'Real Path','Simulated Path',simulated_scaleX},'TextColor', 'k','location','best');hold off;

    figure(22);subplot(2,1,2);set(gca,'fontsize',16);grid on;hold on;
    plot(A.data(:,1),POS3DY,'o-','LineWidth',1);plot(A.data(:,1),SimOutputY_best,'x-','LineWidth',1);plot(A.data(:,1),SimOutputY_best2,'x-','LineWidth',1);
    title(['Y axis',num2str(Ry)],'Color', 'm');xlabel('Time(msec)','Color', 'k');ylabel('Length(mm)','Color', 'k');
    simulated_scaleY=['Simulated Path(Ygain=' num2str(best_GainY) ')'];
    legend({'Real Path','Simulated Path',simulated_scaleY},'TextColor', 'k','location','best');hold off;
%%
for s=1:3   % 加工指標Measured Point
    conerN(nb_times,s) = numel(find(PerformanceReal(1:length(PerformanceReal(:,1)),8)==s)); % 計算Performance(:,s)中出現數字"s"的次數
    RaN(nb_times,s) = numel(find(PerformanceReal(1:length(PerformanceReal(:,1)),9)==s)); % 計算Performance(:,s)中出現數字"s"的次數
    
    figure(30+2*s-1);plot(1:length(PerformanceReal(:,1)),PerformanceReal(:,2*s-1),'*-');
    title(['Corner Error' int2str(s)]);xlabel('Number of Cycles');ylabel('length(mm)');set(gca,'fontsize',16);grid on;hold on;    
    figure(30+2*s);plot(1:length(PerformanceReal(:,1)),PerformanceReal(:,2*s),'*-');
    title(['Vibration Ry', int2str(s)]);xlabel('Number of Cycles');ylabel('length(um)');set(gca,'fontsize',16);grid on;hold on;
end
figure(30+2*s+1);plot(1:length(PerformanceReal(:,1)),PerformanceReal(:,2*s+1),'*-');
title('Cycle Time');xlabel('Number of Cycles');ylabel('Time(ms)');set(gca,'fontsize',16);grid on;hold on;
figure(30+2*s+2);plot(1:length(PerformanceReal(:,1)),PerformanceReal(:,2*s+2),'*');
title('Maximum Corner Error Location');xlabel('Number of Cycles');ylabel('Measured Point');set(gca,'fontsize',16);grid on;hold on;
figure(30+2*s+3);plot(1:length(PerformanceReal(:,1)),PerformanceReal(:,2*s+3),'*');
title('Maximum Vibration Ry Location');xlabel('Number of Cycles');ylabel('Measured Point');set(gca,'fontsize',16);grid on;hold on;
%%
for s=1:3   % 加工指標Measured Point
    conerN(nb_times,s) = numel(find(PerformanceSimu_best(1:length(PerformanceSimu_best(:,1)),8)==s)); % 計算Performance(:,s)中出現數字"s"的次數
    RaN(nb_times,s) = numel(find(PerformanceSimu_best(1:length(PerformanceSimu_best(:,1)),9)==s)); % 計算Performance(:,s)中出現數字"s"的次數
    
    figure(30+2*s-1);plot(1:length(PerformanceSimu_best(:,1)),PerformanceSimu_best(:,2*s-1),'*-');
    figure(30+2*s);plot(1:length(PerformanceSimu_best(:,1)),PerformanceSimu_best(:,2*s),'*-');
end
figure(30+2*s+1);plot(1:length(PerformanceSimu_best(:,1)),PerformanceSimu_best(:,2*s+1),'*-');
figure(30+2*s+2);plot(1:length(PerformanceSimu_best(:,1)),PerformanceSimu_best(:,2*s+2),'*');
figure(30+2*s+3);plot(1:length(PerformanceSimu_best(:,1)),PerformanceSimu_best(:,2*s+3),'*');
%%
for s=1:3   % 加工指標Measured Point
    conerN(nb_times,s) = numel(find(PerformanceSimu_best2(1:length(PerformanceSimu_best2(:,1)),8)==s)); % 計算Performance(:,s)中出現數字"s"的次數
    RaN(nb_times,s) = numel(find(PerformanceSimu_best2(1:length(PerformanceSimu_best2(:,1)),9)==s)); % 計算Performance(:,s)中出現數字"s"的次數
    
    figure(30+2*s-1);plot(1:length(PerformanceSimu_best2(:,1)),PerformanceSimu_best2(:,2*s-1),'*-');legend('Real','Simulated','Simulated(scaled)','location','best');
    figure(30+2*s);plot(1:length(PerformanceSimu_best2(:,1)),PerformanceSimu_best2(:,2*s),'*-');legend('Real','Simulated','Simulated(scaled)','location','best');
end
figure(30+2*s+1);plot(1:length(PerformanceSimu_best2(:,1)),PerformanceSimu_best2(:,2*s+1),'*-');legend('Real','Simulated','Simulated(scaled)','location','best');
figure(30+2*s+2);plot(1:length(PerformanceSimu_best2(:,1)),PerformanceSimu_best2(:,2*s+2),'*');legend('Real','Simulated','Simulated(scaled)','location','best');
figure(30+2*s+3);plot(1:length(PerformanceSimu_best2(:,1)),PerformanceSimu_best2(:,2*s+3),'*');legend('Real','Simulated','Simulated(scaled)','location','best');
%%
Gx10=tf(best_numX,best_denX);Gy10=tf(best_numY,best_denY);
save('Gx15_new','Gx10');save('Gy15_new','Gy10');
end
save;