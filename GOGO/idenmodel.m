function [num den]=idenmodel(AxisFRFamp,AxisFRFpha,oder_num,oder_den)

% 訓練數據
AMP=importdata(AxisFRFamp, ';');dt_X_amp=AMP.data(1:500,:);
% 1.Frequency(Hz) 2.X Open control loop(dB) 3.X Closed control loop(dB) 4.Bandwidth 5.Amplitudemargin 6.Phasemargin
PHASE=importdata(AxisFRFpha, ';');dt_X_phase=PHASE.data(1:500,:);
% 1.Frequency(Hz) 2.X Open control loop(dB) 3.X Closed control loop(dB) 4.Amplitudemargin 5.Phasemargin 6.Phase180 7.Phase120

% % 波德圖轉FRF
for n=1:length(dt_X_amp(:,1))
%     FRF_open(n)=10.^(dt_X_amp(n,2)/20)*cosd(dt_X_phase(n,2))+10.^(dt_X_amp(n,2)/20)*sind(dt_X_phase(n,2))*i;
    FRF_close(n)=10.^(dt_X_amp(n,3)/20)*cosd(dt_X_phase(n,3))+10.^(dt_X_amp(n,3)/20)*sind(dt_X_phase(n,3))*i;
end
% % 波德圖轉FRF

% 旋轉，從0度到360度
% for j=1:length(dt_X_phase(:,1))
%     if dt_X_phase(j,2)>0
%         phase_open(j)=dt_X_phase(j,2)-360;
%     else
%         phase_open(j)=dt_X_phase(j,2);
%     end
% end

for j=1:length(dt_X_phase(:,1))
    if dt_X_phase(j,3)>0
        phase_close(j)=dt_X_phase(j,3)-360;        
    else
        phase_close(j)=dt_X_phase(j,3);        
    end
end
% 旋轉，從0度到360度
        
% [num den]=invfreqs(FRF_close(:),dt_X_amp(:,1)*2*pi,oder_num,oder_den,[],100);

[num den]=invfreqs(FRF_close(:),dt_X_amp(:,1)*2*pi,oder_num,oder_den,[],100);