function z=PathSeparator2(x,y,u,w,s)
% x=x軸座標資料 % y=y軸座標資料 % u=x軸起點判斷點 % w=y軸起點判斷點 
% s=遠離判斷點筆數，最少資料點數圈的50%左右穩定度比較好，不會因為在大而跳到下一圈，也不會太少而無法脫離每圈判斷點範圍。
% 程式說明：利用起點判斷點，在座標資料小於判斷點則紀錄其為第幾筆資料，並往前增加一定筆數以遠離起點判斷點範圍。

closest_point=sort(sqrt((x-u).^2+(y-w).^2)); % 9.884660000000212e-04
count=1;i=1;cycle(1)=1;
while i<=length(x)
    
    if x(i)<=u && y(i)<=w
        
        count=count+1;
        cycle(count)=i;
        i=i+s;
        
        if x(i)<=u && y(i)<=w
            break;
        end
        
    end
    
    i=i+1;
    
end

cycle(count+1)=length(x);

z=cycle;